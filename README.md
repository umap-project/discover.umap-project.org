# discover.umap-project.org

## Fetching sources

First, fetch sources from the main uMap repository:

    $ make fetch

## Installing dependencies

You must have Python3.8+

Create and activate a virtualenv:

    $ python3 -m venv venv
    $ source venv/bin/activate

Install dependencies (depends on the fetch command!):

    $ make install

## Launching locally

Use mkdocs to build and serve the website:

    $ make serve

Go to http://127.0.0.1:8000/
