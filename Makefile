.DEFAULT_GOAL := help
RED=\033[0;31m
GREEN=\033[0;32m
ORANGE=\033[0;33m
BLUE=\033[0;34m
NC=\033[0m # No Color

.PHONY: fetch
fetch: ## Retrieve docs from umap repository
	# See https://gist.github.com/smac89/3d1e3cb1f1b425fa9a400c61c51cff24
	rm -rf docs
	git init tmp-umap
	cd tmp-umap && git remote add origin https://github.com/umap-project/umap
	cd tmp-umap && git fetch --depth=1
	cd tmp-umap && git config core.sparseCheckout true
	echo docs-users >> tmp-umap/.git/info/sparse-checkout
	cd tmp-umap && git checkout master
	mv tmp-umap/docs-users docs
	rm -rf tmp-umap

.PHONY: install
install: ## Install Python dependencies (manually run `fetch` before)
	@echo "${GREEN}🤖 Installing dependencies${NC}"
	python3 -m pip install --upgrade pip
	@echo "${ORANGE}⚠️ Must be done after the fetch command${NC}"
	python3 -m pip install -r docs/requirements.txt
	python3 -m pip install selectolax

.PHONY: serve
serve: ## Generate and serve the site with mkdocs
	mkdocs serve

.PHONY: serve-local
serve-local: ## Generate and serve the site from ../umap/docs-users
	mkdocs serve --config-file mkdocs-local.yml

.PHONY: build
build: ## Build the website for deployment
	mkdocs build

.PHONY: help
help:
	@python3 -c "$$PRINT_HELP_PYSCRIPT" < $(MAKEFILE_LIST)

# See https://daniel.feldroy.com/posts/autodocumenting-makefiles
define PRINT_HELP_PYSCRIPT # start of Python section
import re, sys

output = []
# Loop through the lines in this file
for line in sys.stdin:
    # if the line has a command and a comment start with
    #   two pound signs, add it to the output
    match = re.match(r'^([a-zA-Z_-]+):.*?## (.*)$$', line)
    if match:
        target, help = match.groups()
        output.append("\033[36m%-15s\033[0m %s" % (target, help))
# Sort the output in alphanumeric order
output.sort()
# Print the help result
print('\n'.join(output))
endef
export PRINT_HELP_PYSCRIPT # End of python section
