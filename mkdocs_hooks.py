import subprocess
from pathlib import Path

from selectolax.parser import HTMLParser
from mkdocs.config.defaults import MkDocsConfig
from mkdocs.structure.files import Files
from mkdocs.structure.pages import Page


def generate_image(
    url, output, width, height, wait, wait_for, selector, padding, javascript
):
    params = [
        "shot-scraper",
        "shot",
        url,
        "--output",
        str(output),
        "--retina",
        # "--log-console",
        # "--interactive",
        # "--browser",
        # "firefox",
    ]
    if wait:
        params += ["--wait", wait]
    if wait_for:
        params += ["--wait-for", wait_for]
    # if width:
    #     params += ["--width", width]
    # if height:
    #     params += ["--height", height]
    if selector:
        params += ["--selector", selector]
    if padding:
        params += ["--padding", padding]
    if javascript:
        javascript = " ".join(javascript.strip().split("\n"))
        params += ["--javascript", javascript]
    print("Command:", " ".join(str(param) for param in params))
    subprocess.check_output(params)


def on_page_content(
    html: str, *, page: Page, config: MkDocsConfig, files: Files
) -> str:
    if "<shot-scraper" not in html:
        # Early return, avoids parsing the HTML.
        return html

    shotscraper_force_generate = bool(
        config.get("extra", {}).get("shotscraper_force_generate", False)
    )

    tree = HTMLParser(html)
    src_dir = Path(page.file.src_dir)

    for node in tree.css("shot-scraper"):
        node_attrs = node.attributes
        url = node_attrs.get("data-url")
        output = node_attrs.get("data-output")
        alt = node_attrs.get("data-alt")
        caption = node_attrs.get("data-caption", "")
        absolute_path = src_dir / output
        width = node_attrs.get("data-width")
        height = node_attrs.get("data-height")
        wait = node_attrs.get("data-wait")
        wait_for = node_attrs.get("data-wait-for")
        selector = node_attrs.get("data-selector", "")
        padding = node_attrs.get("data-padding", 0)
        javascript = node_attrs.get("data-javascript")
        if not absolute_path.exists() or shotscraper_force_generate:
            generate_image(
                url,
                absolute_path,
                width,
                height,
                wait,
                wait_for,
                selector,
                padding,
                javascript,
            )
        figure_element = HTMLParser(f"""
            <figure markdown="span">
                <img src="../../../{output}"
                    alt="{alt}" width="{width or 50}" height="{height or 50}">
              <figcaption>{node.child and node.html or caption}</figcaption>
            </figure>
        """)
        node.parent.replace_with(figure_element.body.child)

    return tree.html
